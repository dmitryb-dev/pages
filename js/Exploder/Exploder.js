"use strict";
var Exploder = (function () {
    function Exploder(element, link) {
        if (link === void 0) { link = null; }
        this._element = element;
        this._link = link;
        this._explodedLink = null;
        this._errorHandlers = new Array();
        this._loadListeners = new Array();
        this._progressListeners = new Array();
    }
    Object.defineProperty(Exploder.prototype, "onLoad", {
        set: function (listener) {
            this._loadListeners.push(listener);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Exploder.prototype, "onProgress", {
        set: function (listener) {
            this._progressListeners.push(listener);
        },
        enumerable: true,
        configurable: true
    });
    Exploder.prototype.explode = function () {
        if (this._link == null)
            throw new Error("Nothing to download, link = null");
        this.explodeLink(this._link);
    };
    Exploder.prototype.explodeLink = function (link) {
        var _this = this;
        if (link == this._explodedLink)
            return;
        this._load(link, function (response) {
            _this.explodeHTML(response);
            _this._loadListeners.forEach(function (listener) { return listener(_this._element); });
            _this._explodedLink = link;
        }, function (loaded, content) {
            _this._progressListeners.forEach(function (listner) { return listner(loaded, content); });
        });
    };
    Exploder.prototype.explodeHTML = function (text) {
        var body = text.match(/<body[^>]*>((.|[\n\r])*)<\/body>/im);
        this._element.innerHTML = body ? body[1] : text;
    };
    Exploder.prototype.explodeContent = function (content) {
        this.unload();
        this._element.appendChild(content);
    };
    Exploder.prototype.update = function () {
        var savedLink = this._explodedLink ? this._explodedLink : this._link;
        this.unload();
        if (this._link)
            this.explodeLink(savedLink);
    };
    Exploder.prototype.restore = function () {
        var _this = this;
        if (!this._savedContent)
            return;
        while (this._element.firstChild)
            this._element.removeChild(this._element.firstChild);
        this._savedContent.forEach(function (node) { return _this._element.appendChild(node); });
        this._savedContent = null;
    };
    Exploder.prototype.unload = function () {
        this._savedContent = new Array();
        while (this._element.firstChild) {
            this._savedContent.push(this._element.firstChild);
            this._element.removeChild(this._element.firstChild);
        }
        this._explodedLink = null;
    };
    Object.defineProperty(Exploder.prototype, "onError", {
        set: function (handler) {
            this._errorHandlers.push(this);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Exploder.prototype, "link", {
        get: function () {
            return this._link;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Exploder.prototype, "container", {
        get: function () {
            return this._element;
        },
        enumerable: true,
        configurable: true
    });
    Exploder.prototype._load = function (link, onload, onProgress) {
        var _this = this;
        var request = new XMLHttpRequest();
        request.withCredentials = true;
        request.open("GET", link, true);
        request.addEventListener("load", function () {
            if (request.readyState != request.DONE)
                return;
            if (request.status == 200)
                onload(request.responseText);
            else
                _this._notifyErrorHandlers(link, request.status);
        });
        request.addEventListener("progress", function (event) {
            onProgress(event.loaded / event.total, request.responseText);
        });
        request.send();
    };
    Exploder.prototype._notifyErrorHandlers = function (link, status) {
        this._errorHandlers.forEach(function (handler) { return handler(link, status); });
    };
    return Exploder;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Exploder;
//# sourceMappingURL=Exploder.js.map