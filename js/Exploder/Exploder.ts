export default class Exploder {
    constructor(element: HTMLElement, link: string = null) {
        this._element = element;
        this._link = link;
        this._explodedLink = null;
        this._errorHandlers = new Array();
        this._loadListeners = new Array();
        this._progressListeners = new Array();
    }

    set onLoad(listener) {
        this._loadListeners.push(listener);
    }
    set onProgress(listener) {
        this._progressListeners.push(listener);
    }

    explode() {
        if (this._link == null) throw new Error("Nothing to download, link = null");
        this.explodeLink(this._link);
    }

    explodeLink(link: string) {
        if (link == this._explodedLink) return;
        this._load(link, (response) => {
            this.explodeHTML(response);
            this._loadListeners.forEach((listener) => listener(this._element));
            this._explodedLink = link;
        }, (loaded, content) => {
            this._progressListeners.forEach((listner) => listner(loaded, content));
        });
    }

    explodeHTML(text: string) {
        const body = text.match(/<body[^>]*>((.|[\n\r])*)<\/body>/im);
        this._element.innerHTML = body? body[1] : text;
    }

    explodeContent(content: HTMLElement) {
        this.unload();
        this._element.appendChild(content);
    }

    update() {
        const savedLink = this._explodedLink? this._explodedLink : this._link;
        this.unload();
        if (this._link) this.explodeLink(savedLink);
    }

    private _savedContent: Array<HTMLElement>;
    restore() {
        if (!this._savedContent) return;

        while (this._element.firstChild)
            this._element.removeChild(this._element.firstChild);
        this._savedContent.forEach((node) => this._element.appendChild(node));

        this._savedContent = null;
    }
    unload() {
        this._savedContent = new Array();
        while (this._element.firstChild) {
            this._savedContent.push(this._element.firstChild);
            this._element.removeChild(this._element.firstChild);
        }
        this._explodedLink = null;
    }

    set onError(handler) {
        this._errorHandlers.push(this);
    }

    get link() {
        return this._link;
    }
    get container() {
        return this._element;
    }

    private _load(link, onload, onProgress) {
        const request = new XMLHttpRequest();
        request.withCredentials = true;
        request.open("GET", link, true);
        request.addEventListener("load", () => {
            if (request.readyState != request.DONE) return;
            if (request.status == 200) onload(request.responseText);
            else this._notifyErrorHandlers(link, request.status);
        });
        request.addEventListener("progress", (event) => {
            onProgress(event.loaded / event.total, request.responseText);
        });
        request.send();
    }

    private _notifyErrorHandlers(link, status) {
        this._errorHandlers.forEach((handler) => handler(link, status));
    }

    private _element;
    private _link;
    private _explodedLink;
    private _errorHandlers;
    private _loadListeners;
    private _progressListeners;
}