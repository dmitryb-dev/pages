"use strict";
var Exploder_1 = require("./Exploder");
var PagesHTMLParser_1 = require("../PagesHTMLParser");
var ExplodersParser = (function () {
    function ExplodersParser() {
        this._exploders = new Array();
    }
    ExplodersParser.prototype.get = function (name) {
        for (var i = 0; i < this._exploders.length; i++)
            if (this._exploders[i].name == name)
                return this._exploders[i].exploder;
        return null;
    };
    ExplodersParser.prototype.from = function (node) {
        var _this = this;
        PagesHTMLParser_1.findByAttribute(node, 'div', EXPLODER_ATTRIBUTE).forEach(function (exploderHTML) {
            var link = exploderHTML.getAttribute(EXPLODER_LINK_ATTRIBUTE);
            _this._exploders.push(new ExploderMap(exploderHTML.getAttribute(EXPLODER_ATTRIBUTE), new Exploder_1.default(exploderHTML, link ? link : null)));
        });
    };
    return ExplodersParser;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ExplodersParser;
var ExploderMap = (function () {
    function ExploderMap(name, exploder) {
        this._exploder = exploder;
        this._name = name;
    }
    Object.defineProperty(ExploderMap.prototype, "name", {
        get: function () { return this._name; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExploderMap.prototype, "exploder", {
        get: function () { return this._exploder; },
        enumerable: true,
        configurable: true
    });
    return ExploderMap;
}());
var EXPLODER_ATTRIBUTE = 'remote';
var EXPLODER_LINK_ATTRIBUTE = 'href';
//# sourceMappingURL=ExploderParser.js.map