import Exploder from "./Exploder";
import {findByAttribute} from "../PagesHTMLParser";
export default class ExplodersParser {
    constructor() {
        this._exploders = new Array<ExploderMap>();
    }

    get(name: string): Exploder {
        for (let i = 0; i < this._exploders.length; i++)
            if (this._exploders[i].name == name) return this._exploders[i].exploder;
        return null;
    }
    from(node: Node) {
        findByAttribute(node, 'div', EXPLODER_ATTRIBUTE).forEach((exploderHTML) => {
            const link = exploderHTML.getAttribute(EXPLODER_LINK_ATTRIBUTE);
            this._exploders.push(new ExploderMap(
                exploderHTML.getAttribute(EXPLODER_ATTRIBUTE),
                new Exploder(exploderHTML, link? link : null)
            ));
        });
    }

    private _exploders;
}

class ExploderMap {
    constructor(name, exploder) {
        this._exploder = exploder;
        this._name = name;
    }

    get name() { return this._name; }
    get exploder() { return this._exploder; }
    private _exploder;
    private _name;
}

const EXPLODER_ATTRIBUTE = 'remote';
const EXPLODER_LINK_ATTRIBUTE = 'href';