"use strict";
var Pages_1 = require("./Pages");
var HashPathResolver_1 = require("./core/Controller/HashPathResolver");
var SlashPathResolver_1 = require("./core/Controller/SlashPathResolver");
var PagesFactory = (function () {
    function PagesFactory() {
    }
    PagesFactory.fromDocument = function (resolver) {
        if (resolver === void 0) { resolver = PagesFactory.hashMode(); }
        var pages = new Pages_1.default(resolver);
        pages.parse(document);
        return pages;
    };
    PagesFactory.hashMode = function () { return new HashPathResolver_1.default(); };
    PagesFactory.slashMode = function (origin) {
        if (origin === void 0) { origin = "/"; }
        return new SlashPathResolver_1.default(origin);
    };
    return PagesFactory;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = PagesFactory;
//# sourceMappingURL=PagesFactory.js.map