import {Animation} from "./core/Pages/Animation";
import Pages from "./Pages";
import {IPageResolver} from "./core/Controller/IPageResolver";
import HashPathResolver from "./core/Controller/HashPathResolver";
import SlashPathResolver from "./core/Controller/SlashPathResolver";
import PagesContainer from "./core/Pages/PagesContainer";
import PageBinding from "./core/Pages/PageBinding";
import Page from "./core/Pages/Page";

export default class PagesFactory {

    static fromDocument(resolver: IPageResolver = PagesFactory.hashMode()): Pages {
        const pages = new Pages(resolver);
        pages.parse(document);
        return pages;
    }

    static hashMode() { return new HashPathResolver();}
    static slashMode(origin = "/") { return new SlashPathResolver(origin);}
}



