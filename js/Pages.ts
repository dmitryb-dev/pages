import PagesContainer from "./core/Pages/PagesContainer";
import PagesController from "./core/Controller/PageController";
import {Page as ControllerPage} from "./core/Controller/PageController";
import Page from "./core/Pages/Page";
import Router from "./core/Router";
import {IPageResolver} from "./core/Controller/IPageResolver";
import PageBinding from "./core/Pages/PageBinding";
import * as PagesHTML from "./PagesHTMLParser";
import {PagesHTMLParser as parser} from "./PagesHTMLParser";
export default class Pages {
    constructor(pathResolver) {
        this._containers = new Array;
        this._controller = new PagesController(pathResolver);
        this._router = new Router();
        this._router.onRoute = () => this._controller.handle();
    }

    addContainer(container: PagesContainer) {
        this._containers.push(container);
    }
    container(name: string): PagesContainer {
        for (let i = 0; i < this._containers.length; i++)
            if (this._containers[i].name == name) return this._containers[i];
        return null;
    }
    get controller() { return this._controller; }

    addPage(containerName, page: Page, title: string) {
        const existed = this._controller.page(page.name);
        if (existed) {
            existed.title = title;
        } else {
            const controllerPage = new ControllerPage(page.name, title);
            this._controller.add(controllerPage);
            const container = this.container(containerName);
            container.addPage(page);
            controllerPage.onSelect = () => container.switch(page.name);
        }
    }

    registerLink(link) {
        const href = this._getHref(link.getAttribute(PagesHTML.PAGE_ATTRIBUTE));
        link.addEventListener('click', (e) => {
            if (e.which != 1) return;
            e.preventDefault(); this._router.route(href);
        }, false);
    }
    
    parse(element: Node) {
        parser.findSwitchers(element).forEach((switcher) => {
            const link = switcher.getAttribute(PagesHTML.PAGE_ATTRIBUTE);
            switcher.href = this._controller.pathResolver.pathToPage(link.split('#'));
            this.registerLink(switcher);
        });
        parser.findContainers(element).forEach((containerHTML) => {
            containerHTML.classList.add(PagesHTML.CONTAINER_CLASS);
            const defaultBinding = new PageBinding(null, parser.parseAnimation(containerHTML));
            const container = new PagesContainer(containerHTML.getAttribute(PagesHTML.CONTAINER_ATTRIBUTE), defaultBinding);
            this.addContainer(container);
            parser.findPages(containerHTML).forEach((pageHTML) => {
                const name = pageHTML.getAttribute(PagesHTML.PAGE_ATTRIBUTE);
                let title = pageHTML.getAttribute(PagesHTML.TITLE_ATTRIBUTE);
                if (!title) title = name;
                this.addPage(container.name, new Page(name, pageHTML), title);
            });
        });
    }

    setIndex(name: string) {
        this._controller.index = name;
    }

    start() {
        this._router.route(window.location.href);
    }

    private _getHref(link: string) {
        return this._controller.pathResolver.pathToPage(link.split('#'));
    }

    private _containers;
    private _controller: PagesController;
    private _router: Router;
}

