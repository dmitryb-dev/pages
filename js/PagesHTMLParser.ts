import {Animation} from "./core/Pages/Animation";
export class PagesHTMLParser {
    static findPages(container: HTMLElement): Array<HTMLElement> {
        const pages = new Array<HTMLElement>();

        for (let i = 0; i < container.children.length; i++) {
            const element: HTMLElement = container.children.item(i) as HTMLElement;
            if (element.hasAttribute(PAGE_ATTRIBUTE)) {
                pages.push(element);
                element.classList.add(PAGE_CLASS);
            }
        }
        return pages;
    }

    static parseAnimation(element: HTMLElement, defaultAnimation: Animation = new Animation('', '')): Animation {
        let appear = element.getAttribute(APPEAR_ANIMATION_TAG);
        if (!appear) appear = defaultAnimation.appear;

        let leave = element.getAttribute(LEAVE_ANIMATION_TAG);
        if (!leave) leave = defaultAnimation.leave;

        if (appear == defaultAnimation.appear && leave == defaultAnimation.leave) return null;
        return new Animation(appear, leave);
    }

    static findSwitchers(source) {
        return findByAttribute(source, "a", PAGE_ATTRIBUTE);
    }

    static findContainers(source) {
        return findByAttribute(source, "div", CONTAINER_ATTRIBUTE);
    }

}

export function findByAttribute(source, tag: string, attribute: string) {
    const tags = source.getElementsByTagName(tag);
    const elements = new Array();
    for (let i = 0; i < tags.length; i++) {
        const tag = tags.item(i) as HTMLElement;
        if (tag.hasAttribute(attribute)) elements.push(tag);
    }
    return elements;
}

export const CONTAINER_CLASS = 'pages';
export const CONTAINER_ATTRIBUTE = 'pages';
export const PAGE_ATTRIBUTE = 'page';
export const TITLE_ATTRIBUTE = 'title';
export const PAGE_CLASS = 'page';
export const APPEAR_ANIMATION_TAG = 'appear';
export const LEAVE_ANIMATION_TAG = 'leave';