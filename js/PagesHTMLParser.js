"use strict";
var Animation_1 = require("./core/Pages/Animation");
var PagesHTMLParser = (function () {
    function PagesHTMLParser() {
    }
    PagesHTMLParser.findPages = function (container) {
        var pages = new Array();
        for (var i = 0; i < container.children.length; i++) {
            var element = container.children.item(i);
            if (element.hasAttribute(exports.PAGE_ATTRIBUTE)) {
                pages.push(element);
                element.classList.add(exports.PAGE_CLASS);
            }
        }
        return pages;
    };
    PagesHTMLParser.parseAnimation = function (element, defaultAnimation) {
        if (defaultAnimation === void 0) { defaultAnimation = new Animation_1.Animation('', ''); }
        var appear = element.getAttribute(exports.APPEAR_ANIMATION_TAG);
        if (!appear)
            appear = defaultAnimation.appear;
        var leave = element.getAttribute(exports.LEAVE_ANIMATION_TAG);
        if (!leave)
            leave = defaultAnimation.leave;
        if (appear == defaultAnimation.appear && leave == defaultAnimation.leave)
            return null;
        return new Animation_1.Animation(appear, leave);
    };
    PagesHTMLParser.findSwitchers = function (source) {
        return findByAttribute(source, "a", exports.PAGE_ATTRIBUTE);
    };
    PagesHTMLParser.findContainers = function (source) {
        return findByAttribute(source, "div", exports.CONTAINER_ATTRIBUTE);
    };
    return PagesHTMLParser;
}());
exports.PagesHTMLParser = PagesHTMLParser;
function findByAttribute(source, tag, attribute) {
    var tags = source.getElementsByTagName(tag);
    var elements = new Array();
    for (var i = 0; i < tags.length; i++) {
        var tag_1 = tags.item(i);
        if (tag_1.hasAttribute(attribute))
            elements.push(tag_1);
    }
    return elements;
}
exports.findByAttribute = findByAttribute;
exports.CONTAINER_CLASS = 'pages';
exports.CONTAINER_ATTRIBUTE = 'pages';
exports.PAGE_ATTRIBUTE = 'page';
exports.TITLE_ATTRIBUTE = 'title';
exports.PAGE_CLASS = 'page';
exports.APPEAR_ANIMATION_TAG = 'appear';
exports.LEAVE_ANIMATION_TAG = 'leave';
//# sourceMappingURL=PagesHTMLParser.js.map