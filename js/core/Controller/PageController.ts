import {IPageResolver} from "./IPageResolver";
export default class PagesController {
    constructor(pathResolver: IPageResolver, indexPage: string = null) {
        this._pathResolver = pathResolver;
        this.index = indexPage;
        this._on404 = () => { return true; };
        this._pages = new Array();
    }

    handle(data = null) {
        const pages = this._pathResolver.resolve();
        if (pages == null) return false;
        if (pages.length == 0) {
            if (!this.index) return false;
            history.replaceState(null, this.index, this._pathResolver.pathToPage(new Array(this.index)));
            return this.handle(data);
        }
        let e404pages = new Array();
        pages.forEach((pageName, i) => {
            const name = pageName.replace(/%20/, ' ');
            const page = this.page(name);
            if (page == null) {
                e404pages.push(name);
                return;
            }
            if (i == (pages.length - 1)) document.title = page.title;

            if (page._notifyInterceptors(data)) page._notifyOnSelect(data);
            else return true;
        });
        if (e404pages.length > 0) return this._on404(e404pages);
        return true;
    }

    add(page: Page) {
        this._pages.push(page);
    }
    page(name: string): Page {
        for (let i = 0; i < this._pages.length; i++) {
            if (this._pages[i].name == name) return this._pages[i];
        }
        return null;
    }

    set on404(handler) {
        this._on404 = handler;
    }
    get pathResolver() {
        return this._pathResolver;
    }

    private _pathResolver: IPageResolver;
    index: string;
    private _on404: Function;
    private _pages: Array<Page>;
}

export class Page {
    constructor(name: string, title: string) {
        this._name = name;
        this.title = title;
        this._interceptors = new Array();
        this._handlers = new Array();
    }
    get name() { return this._name; }

    set onSelect(listener) {
        this._handlers.push(listener);
    }
    set onHandle(listener) {
        this._interceptors.push(listener);
    }

    _notifyOnSelect(data = null) {
        this._handlers.forEach((func) => func(data));
    }

    _notifyInterceptors(data = null): Boolean {
        return this._interceptors.every((interceptor) => interceptor(data));
    }

    private _handlers;
    private _interceptors;

    title;
    private _name;
}