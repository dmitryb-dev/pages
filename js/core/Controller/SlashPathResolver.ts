import {IPageResolver} from "./IPageResolver";
export default class SlashPathResolver implements IPageResolver {
    constructor(origin: string = '/') {
        this._origin = getStructure(origin);
    }

    resolve(): Array<string> {
        return this.resolvePath(window.location.pathname);
    }

    resolvePath(path: string): Array<string> {
        return this._getPages(getStructure(path));
    }

    getPageName(): string {
        return this.getPageNameFrom(window.location.pathname);
    }

    getPageNameFrom(path: string): string {
        const structure = this.resolvePath(path);
        return structure.length > 0? structure[structure.length - 1] : null;
    }

    pathToPage(pages: Array<string>) {
        let link = "";
        for (let i = 0; i < pages.length; i++)
            link += pages[i] + "/";
        const structure = getStructure(window.location.pathname);
        const pagesInLocation = this._getPages(structure);
        if (pagesInLocation == null) return null;
        let path = "/";
        for (let i = 0; i < structure.length - pagesInLocation.length; i++)
            path += structure[i] + "/";
        return window.location.origin + path + link;
    }

    get origin() {
        return this._origin;
    }

    _getPages(structure: Array<string>): Array<string> {
        if (this._origin.length == 0)return structure;
        let match = 0;
        let result = new Array();
        for (let i = 0; i < structure.length; i++) {
            if (match >= this._origin.length) {
                result.push(structure[i]);
            }
            else if (structure[i] == this._origin[match]) match++;
        }
        return match == this._origin.length? result : null;
    }
    private _origin: Array<string>;
}

function getStructure(path: string) {
    const structure = new Array<string>();
    path.replace('\\', '/').split(/\/+/g).forEach((dir) => {
        if (dir != '') structure.push(dir.trim());
    });
    return structure;
}