export interface IPageResolver {
    resolve(): Array<string>;
    resolvePath(path: string): Array<string>;

    getPageName(): string;
    getPageNameFrom(path: string): string;

    pathToPage(pages: Array<string>);
}