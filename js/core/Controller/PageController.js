"use strict";
var PagesController = (function () {
    function PagesController(pathResolver, indexPage) {
        if (indexPage === void 0) { indexPage = null; }
        this._pathResolver = pathResolver;
        this.index = indexPage;
        this._on404 = function () { return true; };
        this._pages = new Array();
    }
    PagesController.prototype.handle = function (data) {
        var _this = this;
        if (data === void 0) { data = null; }
        var pages = this._pathResolver.resolve();
        if (pages == null)
            return false;
        if (pages.length == 0) {
            if (!this.index)
                return false;
            history.replaceState(null, this.index, this._pathResolver.pathToPage(new Array(this.index)));
            return this.handle(data);
        }
        var e404pages = new Array();
        pages.forEach(function (pageName, i) {
            var name = pageName.replace(/%20/, ' ');
            var page = _this.page(name);
            if (page == null) {
                e404pages.push(name);
                return;
            }
            if (i == (pages.length - 1))
                document.title = page.title;
            if (page._notifyInterceptors(data))
                page._notifyOnSelect(data);
            else
                return true;
        });
        if (e404pages.length > 0)
            return this._on404(e404pages);
        return true;
    };
    PagesController.prototype.add = function (page) {
        this._pages.push(page);
    };
    PagesController.prototype.page = function (name) {
        for (var i = 0; i < this._pages.length; i++) {
            if (this._pages[i].name == name)
                return this._pages[i];
        }
        return null;
    };
    Object.defineProperty(PagesController.prototype, "on404", {
        set: function (handler) {
            this._on404 = handler;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PagesController.prototype, "pathResolver", {
        get: function () {
            return this._pathResolver;
        },
        enumerable: true,
        configurable: true
    });
    return PagesController;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = PagesController;
var Page = (function () {
    function Page(name, title) {
        this._name = name;
        this.title = title;
        this._interceptors = new Array();
        this._handlers = new Array();
    }
    Object.defineProperty(Page.prototype, "name", {
        get: function () { return this._name; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "onSelect", {
        set: function (listener) {
            this._handlers.push(listener);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "onHandle", {
        set: function (listener) {
            this._interceptors.push(listener);
        },
        enumerable: true,
        configurable: true
    });
    Page.prototype._notifyOnSelect = function (data) {
        if (data === void 0) { data = null; }
        this._handlers.forEach(function (func) { return func(data); });
    };
    Page.prototype._notifyInterceptors = function (data) {
        if (data === void 0) { data = null; }
        return this._interceptors.every(function (interceptor) { return interceptor(data); });
    };
    return Page;
}());
exports.Page = Page;
//# sourceMappingURL=PageController.js.map