"use strict";
var SlashPathResolver = (function () {
    function SlashPathResolver(origin) {
        if (origin === void 0) { origin = '/'; }
        this._origin = getStructure(origin);
    }
    SlashPathResolver.prototype.resolve = function () {
        return this.resolvePath(window.location.pathname);
    };
    SlashPathResolver.prototype.resolvePath = function (path) {
        return this._getPages(getStructure(path));
    };
    SlashPathResolver.prototype.getPageName = function () {
        return this.getPageNameFrom(window.location.pathname);
    };
    SlashPathResolver.prototype.getPageNameFrom = function (path) {
        var structure = this.resolvePath(path);
        return structure.length > 0 ? structure[structure.length - 1] : null;
    };
    SlashPathResolver.prototype.pathToPage = function (pages) {
        var link = "";
        for (var i = 0; i < pages.length; i++)
            link += pages[i] + "/";
        var structure = getStructure(window.location.pathname);
        var pagesInLocation = this._getPages(structure);
        if (pagesInLocation == null)
            return null;
        var path = "/";
        for (var i = 0; i < structure.length - pagesInLocation.length; i++)
            path += structure[i] + "/";
        return window.location.origin + path + link;
    };
    Object.defineProperty(SlashPathResolver.prototype, "origin", {
        get: function () {
            return this._origin;
        },
        enumerable: true,
        configurable: true
    });
    SlashPathResolver.prototype._getPages = function (structure) {
        if (this._origin.length == 0)
            return structure;
        var match = 0;
        var result = new Array();
        for (var i = 0; i < structure.length; i++) {
            if (match >= this._origin.length) {
                result.push(structure[i]);
            }
            else if (structure[i] == this._origin[match])
                match++;
        }
        return match == this._origin.length ? result : null;
    };
    return SlashPathResolver;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = SlashPathResolver;
function getStructure(path) {
    var structure = new Array();
    path.replace('\\', '/').split(/\/+/g).forEach(function (dir) {
        if (dir != '')
            structure.push(dir.trim());
    });
    return structure;
}
//# sourceMappingURL=SlashPathResolver.js.map