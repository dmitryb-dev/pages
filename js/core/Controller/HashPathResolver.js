"use strict";
var HashPathResolver = (function () {
    function HashPathResolver() {
    }
    HashPathResolver.prototype.resolve = function () {
        return this.resolvePath(window.location.hash);
    };
    HashPathResolver.prototype.resolvePath = function (path) {
        // check if path is empty
        if (path.length == 0 || path.match(/^(\s|\/|\\)*$/))
            return Array();
        // get Path #page1#page2 like array: "#page1", "#page2"...
        var hashList = path.match(/#.+?(?=#|$)/g);
        // remove "#" symbol;
        return hashList ? hashList.map(function (hash) { return hash.substr(1); }) : null;
    };
    HashPathResolver.prototype.getPageName = function () {
        return this.getPageNameFrom(window.location.hash);
    };
    HashPathResolver.prototype.getPageNameFrom = function (path) {
        // find last hash tag
        var hash = path.match(/#(.+$)/);
        return hash ? hash[1] : null;
    };
    HashPathResolver.prototype.pathToPage = function (pages) {
        var link = "";
        for (var i = 0; i < pages.length; i++)
            link += '#' + pages[i];
        return link;
    };
    return HashPathResolver;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = HashPathResolver;
//# sourceMappingURL=HashPathResolver.js.map