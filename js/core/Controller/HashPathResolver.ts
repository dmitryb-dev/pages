import {IPageResolver} from "./IPageResolver";
export default class HashPathResolver implements IPageResolver {
    resolve(): Array<string> {
        return this.resolvePath(window.location.hash);
    }

    resolvePath(path:string): Array<string> {
        // check if path is empty
        if (path.length == 0 || path.match(/^(\s|\/|\\)*$/)) return Array();

        // get Path #page1#page2 like array: "#page1", "#page2"...
        const hashList = path.match(/#.+?(?=#|$)/g);

        // remove "#" symbol;
        return hashList? hashList.map((hash) => hash.substr(1)) : null;
    }

    getPageName(): string {
        return this.getPageNameFrom(window.location.hash);
    }

    getPageNameFrom(path: string): string {
        // find last hash tag
        const hash = path.match(/#(.+$)/);

        return hash? hash[1] : null;
    }
    pathToPage(pages: Array<string>) {
        let link = "";
        for (let i = 0; i < pages.length; i++)
            link += '#' + pages[i];
        return link;
    }
}