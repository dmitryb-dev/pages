export default class Router {
    constructor() {
        this._pathHandlers = new Array();
        window.addEventListener('popstate', () => {
            this._routeWithoutPush();
        }, false);
    }

    route(path) {
        history.pushState(null, path, path);
        if (!this._routeWithoutPush()) location.reload();
    }
    private _routeWithoutPush() {
        return this._pathHandlers.some((handler) => handler());
    }
    
    set onRoute(handler) {
        this._pathHandlers.push(handler);
    }

    private _pathHandlers;
}