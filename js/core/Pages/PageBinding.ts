import Page from "./Page";
import {Animation} from "./Animation";
export default class PageBinding {
    constructor(to: string, animation: Animation = null) {
        this._to = to;
        this.animation = animation;
    }
    get to() : string { return this._to; }
    animation: Animation;

    private _to;
}