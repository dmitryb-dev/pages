export interface IAnimation {
    appear: string;
    leave: string;
    appearDelay: number;
}

export class Animation implements IAnimation {
    constructor(appear: string, leave: string, appearDelay: number = 0) {
        this.appear = appear;
        this.leave = leave;
        this.appearDelay = appearDelay;
    }

    appear: string;
    leave: string;
    appearDelay: number;

    clone(): Animation {
        return new Animation(this.appear, this.leave);
    }

    static animate(element: HTMLElement, animation: string,
                   onEnd: Function = empty,
                   onStart: Function = empty,
                   delay: number = 0) {
        setTimeout(() => {
            element.className += ' ' + animation;
            onStart();
            if (isAnimationStarted(element)) {
                once(element, 'animationend', () => {
                    onEnd();
                    removeClasses(element, animation);
                });
            } else {
                onEnd();
                removeClasses(element, animation);
            }
        }, delay);
    }

    static empty() {
        return emptyAnimation;
    }
}

const emptyAnimation = new Animation('', '', 0);

function empty() {}

function isAnimationStarted(element: HTMLElement) {
    const style = getComputedStyle(element, null);
    return style.animationName != 'none';
}

function removeClasses(element: HTMLElement, classes: string) {
    const classesArray = classes.split(' ');
    classesArray.forEach((className) => {
        if (className) element.classList.remove(className)
    });
}

function once(element: HTMLElement, event: string, listener) {
    const handler = function () {
        listener();
        element.removeEventListener(event, handler);
    }
    element.addEventListener(event, handler);
}