"use strict";
var PageBinding = (function () {
    function PageBinding(to, animation) {
        if (animation === void 0) { animation = null; }
        this._to = to;
        this.animation = animation;
    }
    Object.defineProperty(PageBinding.prototype, "to", {
        get: function () { return this._to; },
        enumerable: true,
        configurable: true
    });
    return PageBinding;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = PageBinding;
//# sourceMappingURL=PageBinding.js.map