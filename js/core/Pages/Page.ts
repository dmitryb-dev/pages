import PagesContainer from "./PagesContainer";
import PageBinding from "./PageBinding";
import {IPagesContainer} from "./PagesContainer";
import {Animation} from "./Animation";
export default class Page {
    constructor(name: string, element: HTMLElement) {
        this._name = name;
        this._element = element;
        this._onSelectListeners = new Array();
        this._onLeaveListeners = new Array();
        this._onLoadListeners = new Array();
        this._onUnloadListeners = new Array();
        this._pageBindings = new Array<PageBinding>();
        this._hide();
    }

    get name() { return this._name; }
    get htmlElement() { return this._element; }

    set onLoad(listener) {
        this._onLoadListeners.push(listener);
    }
    set onUnload(listener) {
        this._onUnloadListeners.push(listener);
    }
    set onSelect(listener) {
        this._onSelectListeners.push(listener);
    }
    set onLeave(listener) {
        this._onLeaveListeners.push(listener);
    }

    forEach(pageBindingHandler) {
        this._pageBindings.forEach(pageBindingHandler);
    }
    to(pageName: string): PageBinding {
        const existedBinding = this.pageBindingFor(pageName);
        if (!existedBinding) {
            const binding = new PageBinding(pageName);
            this._pageBindings.push(binding);
            return binding;
        }
        return existedBinding;
    }
    pageBindingFor(pageName: string) {
        for (let i = 0; i < this._pageBindings.length; i++)
            if (this._pageBindings[i].to == pageName) return this._pageBindings[i];
        return null;
    }

    load() {
        this._notify(this._onLoadListeners);
    }
    unload() {
        this._notify(this._onUnloadListeners);
    }

    private _savedPositionState;
    fixed() {
        this._savedPositionState = getComputedStyle(this._element, null).position;
        this._element.style.position = "fixed";
    }
    restorePositionStyle() {
        this._element.style.position = this._savedPositionState;
    }

    private _savedDisplayState;

    show() {
    //    this._element.style.visibility = 'inherit';
        this._element.style.display = this._savedDisplayState;
        this._notify(this._onSelectListeners);
    }
    hide() {
        this._hide();
        this._notify(this._onLeaveListeners);
    }
    private _hide() {
        //    this._element.style.visibility = 'hidden';
        this._savedDisplayState = getComputedStyle(this._element, null).display;
        this._element.style.display = "none";
    }

    private _notify(listeners: Array<Function>) {
        listeners.forEach((listener) => listener(this));
    }

    private _name: string;
    private _element: HTMLElement;
    private _onSelectListeners;
    private _onLeaveListeners;
    private _onLoadListeners;
    private _onUnloadListeners;
    private _pageBindings: Array<PageBinding>;
}

