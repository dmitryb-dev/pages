"use strict";
var Animation = (function () {
    function Animation(appear, leave, appearDelay) {
        if (appearDelay === void 0) { appearDelay = 0; }
        this.appear = appear;
        this.leave = leave;
        this.appearDelay = appearDelay;
    }
    Animation.prototype.clone = function () {
        return new Animation(this.appear, this.leave);
    };
    Animation.animate = function (element, animation, onEnd, onStart, delay) {
        if (onEnd === void 0) { onEnd = empty; }
        if (onStart === void 0) { onStart = empty; }
        if (delay === void 0) { delay = 0; }
        setTimeout(function () {
            element.className += ' ' + animation;
            onStart();
            if (isAnimationStarted(element)) {
                once(element, 'animationend', function () {
                    onEnd();
                    removeClasses(element, animation);
                });
            }
            else {
                onEnd();
                removeClasses(element, animation);
            }
        }, delay);
    };
    Animation.empty = function () {
        return emptyAnimation;
    };
    return Animation;
}());
exports.Animation = Animation;
var emptyAnimation = new Animation('', '', 0);
function empty() { }
function isAnimationStarted(element) {
    var style = getComputedStyle(element, null);
    return style.animationName != 'none';
}
function removeClasses(element, classes) {
    var classesArray = classes.split(' ');
    classesArray.forEach(function (className) {
        if (className)
            element.classList.remove(className);
    });
}
function once(element, event, listener) {
    var handler = function () {
        listener();
        element.removeEventListener(event, handler);
    };
    element.addEventListener(event, handler);
}
//# sourceMappingURL=Animation.js.map