import Page from "./Page";
import PageBinding from "./PageBinding";
import {Animation} from "./Animation";

export interface IPagesContainer {
    switch(name);
}

export default class PagesContainer implements IPagesContainer {
    constructor(name: string, defaultBinding: PageBinding = new PageBinding(null)) {
        this._name = name;
        this._defaultBinding = defaultBinding;
        this._pages = new Array<Page>();
    }

    get name() { return this._name; }
    get pages(): Array<Page> { return this._pages; }
    get defaultBinding() { return this._defaultBinding; }
    get currentPage(): Page { return this._currentPage; }
    forEach(pageHandler) {
        this._pages.forEach(pageHandler);
    }

    page(name: string): Page {
        for (let i = 0; i < this.pages.length; i++)
            if (this.pages[i].name == name) return this.pages[i];
        return null;
    }

    addPage(page: Page) {
        this._pages.push(page);
        page.load();
    }
    removePage(name: string) {
        for (let i = 0; i < this._pages.length; i++)
            if (this._pages[i].name == name) {
                this._pages[i].unload();
                this._pages.splice(i, 1);
                return;
            }
    }

    switch(name: string) {
        if (this._currentPage == null) return this.select(name);
        if (this._currentPage.name == name) return;

        const binding = this._currentPage.pageBindingFor(name);
        const animation = binding? binding.animation : this._defaultBinding.animation;
        if (animation) this.selectWithAnimation(name, animation);
        else this.select(name);
    }
    select(name: string) {
        this.selectWithAnimation(name, Animation.empty());
    }
    selectWithAnimation(name: string, animation: Animation) {
        const newPage = this.page(name);
        if (newPage == null) throw new Error(noSuchPageErrorText(name, this.pages));
        const currentPage = this._currentPage;
        this._currentPage = newPage;

        if (currentPage) {
            // Check if user selects the save page before animation start
            if (newPage.name == currentPage.name) return;
            Animation.animate(currentPage.htmlElement, animation.leave,
                () => {
                    // Check if user not select the same page during animation
                    if (currentPage != this._currentPage) {
                        currentPage.hide();
                        currentPage.fixed();
                    }
                });
        }
        Animation.animate(newPage.htmlElement, animation.appear,
            () => { newPage.restorePositionStyle(); scrollTo(0, 0); },
            () => newPage.show(),
            animation.appearDelay);
    }

    private _name: string;
    private _defaultBinding: PageBinding;
    private _pages: Array<Page>;
    private _currentPage: Page;
}


function noSuchPageErrorText(userWantedPage: string, existedPages: Array<Page>): string {
    const message = 'No such page. You try to switch to page \'' + userWantedPage
        + '\', so we have only ' + existedPages.length + ' pages: {\n';
    const names = existedPages
        .map((item) => item.name)
        .reduce((names, pageName, i) => {
            const prev = i == 1? ('  ' + names + ',\n') : names;
            return prev + '  ' + pageName + ',\n'
        });
    return message + names + '}\n';
}