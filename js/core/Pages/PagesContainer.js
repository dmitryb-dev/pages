"use strict";
var PageBinding_1 = require("./PageBinding");
var Animation_1 = require("./Animation");
var PagesContainer = (function () {
    function PagesContainer(name, defaultBinding) {
        if (defaultBinding === void 0) { defaultBinding = new PageBinding_1.default(null); }
        this._name = name;
        this._defaultBinding = defaultBinding;
        this._pages = new Array();
    }
    Object.defineProperty(PagesContainer.prototype, "name", {
        get: function () { return this._name; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PagesContainer.prototype, "pages", {
        get: function () { return this._pages; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PagesContainer.prototype, "defaultBinding", {
        get: function () { return this._defaultBinding; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PagesContainer.prototype, "currentPage", {
        get: function () { return this._currentPage; },
        enumerable: true,
        configurable: true
    });
    PagesContainer.prototype.forEach = function (pageHandler) {
        this._pages.forEach(pageHandler);
    };
    PagesContainer.prototype.page = function (name) {
        for (var i = 0; i < this.pages.length; i++)
            if (this.pages[i].name == name)
                return this.pages[i];
        return null;
    };
    PagesContainer.prototype.addPage = function (page) {
        this._pages.push(page);
        page.load();
    };
    PagesContainer.prototype.removePage = function (name) {
        for (var i = 0; i < this._pages.length; i++)
            if (this._pages[i].name == name) {
                this._pages[i].unload();
                this._pages.splice(i, 1);
                return;
            }
    };
    PagesContainer.prototype.switch = function (name) {
        if (this._currentPage == null)
            return this.select(name);
        if (this._currentPage.name == name)
            return;
        var binding = this._currentPage.pageBindingFor(name);
        var animation = binding ? binding.animation : this._defaultBinding.animation;
        if (animation)
            this.selectWithAnimation(name, animation);
        else
            this.select(name);
    };
    PagesContainer.prototype.select = function (name) {
        this.selectWithAnimation(name, Animation_1.Animation.empty());
    };
    PagesContainer.prototype.selectWithAnimation = function (name, animation) {
        var _this = this;
        var newPage = this.page(name);
        if (newPage == null)
            throw new Error(noSuchPageErrorText(name, this.pages));
        var currentPage = this._currentPage;
        this._currentPage = newPage;
        if (currentPage) {
            // Check if user selects the save page before animation start
            if (newPage.name == currentPage.name)
                return;
            Animation_1.Animation.animate(currentPage.htmlElement, animation.leave, function () {
                // Check if user not select the same page during animation
                if (currentPage != _this._currentPage) {
                    currentPage.hide();
                    currentPage.fixed();
                }
            });
        }
        Animation_1.Animation.animate(newPage.htmlElement, animation.appear, function () { newPage.restorePositionStyle(); scrollTo(0, 0); }, function () { return newPage.show(); }, animation.appearDelay);
    };
    return PagesContainer;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = PagesContainer;
function noSuchPageErrorText(userWantedPage, existedPages) {
    var message = 'No such page. You try to switch to page \'' + userWantedPage
        + '\', so we have only ' + existedPages.length + ' pages: {\n';
    var names = existedPages
        .map(function (item) { return item.name; })
        .reduce(function (names, pageName, i) {
        var prev = i == 1 ? ('  ' + names + ',\n') : names;
        return prev + '  ' + pageName + ',\n';
    });
    return message + names + '}\n';
}
//# sourceMappingURL=PagesContainer.js.map