"use strict";
var PageBinding_1 = require("./PageBinding");
var Page = (function () {
    function Page(name, element) {
        this._name = name;
        this._element = element;
        this._onSelectListeners = new Array();
        this._onLeaveListeners = new Array();
        this._onLoadListeners = new Array();
        this._onUnloadListeners = new Array();
        this._pageBindings = new Array();
        this._hide();
    }
    Object.defineProperty(Page.prototype, "name", {
        get: function () { return this._name; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "htmlElement", {
        get: function () { return this._element; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "onLoad", {
        set: function (listener) {
            this._onLoadListeners.push(listener);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "onUnload", {
        set: function (listener) {
            this._onUnloadListeners.push(listener);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "onSelect", {
        set: function (listener) {
            this._onSelectListeners.push(listener);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "onLeave", {
        set: function (listener) {
            this._onLeaveListeners.push(listener);
        },
        enumerable: true,
        configurable: true
    });
    Page.prototype.forEach = function (pageBindingHandler) {
        this._pageBindings.forEach(pageBindingHandler);
    };
    Page.prototype.to = function (pageName) {
        var existedBinding = this.pageBindingFor(pageName);
        if (!existedBinding) {
            var binding = new PageBinding_1.default(pageName);
            this._pageBindings.push(binding);
            return binding;
        }
        return existedBinding;
    };
    Page.prototype.pageBindingFor = function (pageName) {
        for (var i = 0; i < this._pageBindings.length; i++)
            if (this._pageBindings[i].to == pageName)
                return this._pageBindings[i];
        return null;
    };
    Page.prototype.load = function () {
        this._notify(this._onLoadListeners);
    };
    Page.prototype.unload = function () {
        this._notify(this._onUnloadListeners);
    };
    Page.prototype.fixed = function () {
        this._savedPositionState = getComputedStyle(this._element, null).position;
        this._element.style.position = "fixed";
    };
    Page.prototype.restorePositionStyle = function () {
        this._element.style.position = this._savedPositionState;
    };
    Page.prototype.show = function () {
        //    this._element.style.visibility = 'inherit';
        this._element.style.display = this._savedDisplayState;
        this._notify(this._onSelectListeners);
    };
    Page.prototype.hide = function () {
        this._hide();
        this._notify(this._onLeaveListeners);
    };
    Page.prototype._hide = function () {
        //    this._element.style.visibility = 'hidden';
        this._savedDisplayState = getComputedStyle(this._element, null).display;
        this._element.style.display = "none";
    };
    Page.prototype._notify = function (listeners) {
        var _this = this;
        listeners.forEach(function (listener) { return listener(_this); });
    };
    return Page;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Page;
//# sourceMappingURL=Page.js.map