"use strict";
var Router = (function () {
    function Router() {
        var _this = this;
        this._pathHandlers = new Array();
        window.addEventListener('popstate', function () {
            _this._routeWithoutPush();
        }, false);
    }
    Router.prototype.route = function (path) {
        history.pushState(null, path, path);
        if (!this._routeWithoutPush())
            location.reload();
    };
    Router.prototype._routeWithoutPush = function () {
        return this._pathHandlers.some(function (handler) { return handler(); });
    };
    Object.defineProperty(Router.prototype, "onRoute", {
        set: function (handler) {
            this._pathHandlers.push(handler);
        },
        enumerable: true,
        configurable: true
    });
    return Router;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Router;
//# sourceMappingURL=Router.js.map