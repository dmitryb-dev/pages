"use strict";
var PagesContainer_1 = require("./core/Pages/PagesContainer");
var PageController_1 = require("./core/Controller/PageController");
var PageController_2 = require("./core/Controller/PageController");
var Page_1 = require("./core/Pages/Page");
var Router_1 = require("./core/Router");
var PageBinding_1 = require("./core/Pages/PageBinding");
var PagesHTML = require("./PagesHTMLParser");
var PagesHTMLParser_1 = require("./PagesHTMLParser");
var Pages = (function () {
    function Pages(pathResolver) {
        var _this = this;
        this._containers = new Array;
        this._controller = new PageController_1.default(pathResolver);
        this._router = new Router_1.default();
        this._router.onRoute = function () { return _this._controller.handle(); };
    }
    Pages.prototype.addContainer = function (container) {
        this._containers.push(container);
    };
    Pages.prototype.container = function (name) {
        for (var i = 0; i < this._containers.length; i++)
            if (this._containers[i].name == name)
                return this._containers[i];
        return null;
    };
    Object.defineProperty(Pages.prototype, "controller", {
        get: function () { return this._controller; },
        enumerable: true,
        configurable: true
    });
    Pages.prototype.addPage = function (containerName, page, title) {
        var existed = this._controller.page(page.name);
        if (existed) {
            existed.title = title;
        }
        else {
            var controllerPage = new PageController_2.Page(page.name, title);
            this._controller.add(controllerPage);
            var container_1 = this.container(containerName);
            container_1.addPage(page);
            controllerPage.onSelect = function () { return container_1.switch(page.name); };
        }
    };
    Pages.prototype.registerLink = function (link) {
        var _this = this;
        var href = this._getHref(link.getAttribute(PagesHTML.PAGE_ATTRIBUTE));
        link.addEventListener('click', function (e) {
            if (e.which != 1)
                return;
            e.preventDefault();
            _this._router.route(href);
        }, false);
    };
    Pages.prototype.parse = function (element) {
        var _this = this;
        PagesHTMLParser_1.PagesHTMLParser.findSwitchers(element).forEach(function (switcher) {
            var link = switcher.getAttribute(PagesHTML.PAGE_ATTRIBUTE);
            switcher.href = _this._controller.pathResolver.pathToPage(link.split('#'));
            _this.registerLink(switcher);
        });
        PagesHTMLParser_1.PagesHTMLParser.findContainers(element).forEach(function (containerHTML) {
            containerHTML.classList.add(PagesHTML.CONTAINER_CLASS);
            var defaultBinding = new PageBinding_1.default(null, PagesHTMLParser_1.PagesHTMLParser.parseAnimation(containerHTML));
            var container = new PagesContainer_1.default(containerHTML.getAttribute(PagesHTML.CONTAINER_ATTRIBUTE), defaultBinding);
            _this.addContainer(container);
            PagesHTMLParser_1.PagesHTMLParser.findPages(containerHTML).forEach(function (pageHTML) {
                var name = pageHTML.getAttribute(PagesHTML.PAGE_ATTRIBUTE);
                var title = pageHTML.getAttribute(PagesHTML.TITLE_ATTRIBUTE);
                if (!title)
                    title = name;
                _this.addPage(container.name, new Page_1.default(name, pageHTML), title);
            });
        });
    };
    Pages.prototype.setIndex = function (name) {
        this._controller.index = name;
    };
    Pages.prototype.start = function () {
        this._router.route(window.location.href);
    };
    Pages.prototype._getHref = function (link) {
        return this._controller.pathResolver.pathToPage(link.split('#'));
    };
    return Pages;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Pages;
//# sourceMappingURL=Pages.js.map