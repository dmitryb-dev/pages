/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var Animation_1 = __webpack_require__(1);
	var Pages_1 = __webpack_require__(2);
	var HashPathResolver_1 = __webpack_require__(5);
	var SlashPathResolver_1 = __webpack_require__(6);
	var PagesContainer_1 = __webpack_require__(7);
	var PageBinding_1 = __webpack_require__(8);
	var Page_1 = __webpack_require__(9);
	var PagesFactory = function () {
	    function PagesFactory() {}
	    PagesFactory.fromDocument = function (resolver) {
	        if (resolver === void 0) {
	            resolver = PagesFactory.hashMode();
	        }
	        var pages = new Pages_1.default(resolver);
	        findContainers().forEach(function (containerHTML) {
	            containerHTML.classList.add(CONTAINER_CLASS);
	            var defaultBinding = new PageBinding_1.default(null, parseAnimation(containerHTML));
	            var container = new PagesContainer_1.default(containerHTML.getAttribute(CONTAINER_ATTRIBUTE), defaultBinding);
	            pages.addContainer(container);
	            findPages(containerHTML).forEach(function (pageHTML) {
	                var name = pageHTML.getAttribute(PAGE_ATTRIBUTE);
	                var title = pageHTML.getAttribute(TITLE_ATTRIBUTE);
	                if (!title) title = name;
	                pages.addPage(container.name, new Page_1.default(name, pageHTML), title);
	            });
	        });
	        findSwitchers().forEach(function (switcher) {
	            var link = switcher.getAttribute(PAGE_ATTRIBUTE);
	            switcher.href = link;
	            pages.registerLink(link);
	        });
	        return pages;
	    };
	    PagesFactory.hashMode = function () {
	        return new HashPathResolver_1.default();
	    };
	    PagesFactory.slashMode = function () {
	        return new SlashPathResolver_1.default();
	    };
	    return PagesFactory;
	}();
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = PagesFactory;
	function findPages(container) {
	    var pages = new Array();
	    for (var i = 0; i < container.children.length; i++) {
	        var element = container.children.item(i);
	        if (element.hasAttribute(PAGE_ATTRIBUTE)) {
	            pages.push(element);
	            element.classList.add(PAGE_CLASS);
	        }
	    }
	    return pages;
	}
	function parseAnimation(element, defaultAnimation) {
	    if (defaultAnimation === void 0) {
	        defaultAnimation = new Animation_1.Animation('', '');
	    }
	    var appear = element.getAttribute(APPEAR_ANIMATION_TAG);
	    if (!appear) appear = defaultAnimation.appear;
	    var leave = element.getAttribute(LEAVE_ANIMATION_TAG);
	    if (!leave) leave = defaultAnimation.leave;
	    if (appear == defaultAnimation.appear && leave == defaultAnimation.leave) return null;
	    return new Animation_1.Animation(appear, leave);
	}
	function findByAttribute(tag, attribute) {
	    var tags = document.getElementsByTagName(tag);
	    var elements = new Array();
	    for (var i = 0; i < tags.length; i++) {
	        var tag_1 = tags.item(i);
	        if (tag_1.hasAttribute(attribute)) elements.push(tag_1);
	    }
	    return elements;
	}
	function findSwitchers() {
	    return findByAttribute("a", PAGE_ATTRIBUTE);
	}
	function findContainers() {
	    return findByAttribute("div", CONTAINER_ATTRIBUTE);
	}
	var CONTAINER_CLASS = 'pages';
	var CONTAINER_ATTRIBUTE = 'pages';
	var PAGE_ATTRIBUTE = 'page';
	var TITLE_ATTRIBUTE = 'title';
	var PAGE_CLASS = 'page';
	var APPEAR_ANIMATION_TAG = 'appear';
	var LEAVE_ANIMATION_TAG = 'leave';
	//# sourceMappingURL=PagesFactory.js.map

/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";
	
	var Animation = function () {
	    function Animation(appear, leave, appearDelay) {
	        if (appearDelay === void 0) {
	            appearDelay = 0;
	        }
	        this.appear = appear;
	        this.leave = leave;
	        this.appearDelay = appearDelay;
	    }
	    Animation.prototype.clone = function () {
	        return new Animation(this.appear, this.leave);
	    };
	    Animation.animate = function (element, animation, onEnd, onStart, delay) {
	        if (onEnd === void 0) {
	            onEnd = empty;
	        }
	        if (onStart === void 0) {
	            onStart = empty;
	        }
	        if (delay === void 0) {
	            delay = 0;
	        }
	        setTimeout(function () {
	            element.className += ' ' + animation;
	            if (isAnimationStarted(element)) {
	                once(element, 'animationstart', function () {
	                    return onStart();
	                });
	                once(element, 'animationend', function () {
	                    return onEnd();
	                });
	            } else {
	                onStart();
	                onEnd();
	            }
	            removeClasses(element, animation);
	        }, delay);
	    };
	    Animation.empty = function () {
	        return emptyAnimation;
	    };
	    return Animation;
	}();
	exports.Animation = Animation;
	var emptyAnimation = new Animation('', '', 0);
	function empty() {}
	function isAnimationStarted(element) {
	    var style = getComputedStyle(element, null);
	    return style.getPropertyValue('animation-name') != 'none';
	}
	function removeClasses(element, classes) {
	    var classesArray = classes.split(' ');
	    classesArray.forEach(function (className) {
	        if (className) element.classList.remove(className);
	    });
	}
	function once(element, event, listener) {
	    var handler = function handler() {
	        listener();
	        element.removeEventListener(event, handler);
	    };
	    element.addEventListener(event, handler);
	}
	//# sourceMappingURL=Animation.js.map

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var PageController_1 = __webpack_require__(3);
	var PageController_2 = __webpack_require__(3);
	var Router_1 = __webpack_require__(4);
	var Pages = function () {
	    function Pages(pathResolver) {
	        var _this = this;
	        this._containers = new Array();
	        this._controller = new PageController_1.default(pathResolver);
	        this._router = new Router_1.default();
	        this._router.onRoute = function () {
	            return _this._controller.handle();
	        };
	    }
	    Pages.prototype.addContainer = function (container) {
	        this._containers.push(container);
	    };
	    Pages.prototype.container = function (name) {
	        for (var i = 0; i < this._containers.length; i++) {
	            if (this._containers[i].name == name) return this._containers[i];
	        }return null;
	    };
	    Object.defineProperty(Pages.prototype, "controller", {
	        get: function get() {
	            return this._controller;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Pages.prototype.addPage = function (containerName, page, title) {
	        var existed = this._controller.page(page.name);
	        if (existed) {
	            existed.title = title;
	        } else {
	            var controllerPage = new PageController_2.Page(page.name, title);
	            this._controller.add(controllerPage);
	            var container_1 = this.container(containerName);
	            container_1.addPage(page);
	            controllerPage.onSelect = function () {
	                return container_1.switch(page.name);
	            };
	        }
	    };
	    Pages.prototype.registerLink = function (link) {
	        var _this = this;
	        var href = this._getHref(link.href);
	        link.addEventListener('click', function (e) {
	            e.preventDefault();_this._router.route(href);
	        }, false);
	    };
	    Pages.prototype.setIndex = function (name) {
	        this._controller.index = this._getHref(name);
	    };
	    Pages.prototype.start = function () {
	        this._router.route(window.location.href);
	    };
	    Pages.prototype._getHref = function (link) {
	        return this._controller.pathResolver.pathToPage(link.split('#'));
	    };
	    return Pages;
	}();
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = Pages;
	//# sourceMappingURL=Pages.js.map

/***/ },
/* 3 */
/***/ function(module, exports) {

	"use strict";
	
	var PagesController = function () {
	    function PagesController(pathResolver, indexPage) {
	        if (indexPage === void 0) {
	            indexPage = null;
	        }
	        this._pathResolver = pathResolver;
	        this._index = indexPage;
	        this._on404 = function () {
	            return true;
	        };
	        this._pages = new Array();
	        if (!indexPage) throw new Error("You must set index page!");
	    }
	    PagesController.prototype.handle = function (data) {
	        var _this = this;
	        if (data === void 0) {
	            data = null;
	        }
	        var pages = this._pathResolver.resolve();
	        if (pages == null) return false;
	        if (pages.length == 0) {
	            if (!this._index) return false;
	            history.replaceState(null, this._index, this._pathResolver.pathToPage(this._index));
	            return this.handle(data);
	        }
	        var e404pages = new Array();
	        pages.forEach(function (pageName, i) {
	            var name = pageName.replace(/%20/, ' ');
	            var page = _this.page(name);
	            if (page == null) e404pages.push(name);
	            if (i == pages.length - 1) document.title = page.title;
	            if (page._notifyInterceptors(data)) page._notifyOnSelect(data);else return true;
	        });
	        if (e404pages.length > 0) return this._on404(e404pages);
	        return true;
	    };
	    PagesController.prototype.add = function (page) {
	        this._pages.push(page);
	    };
	    PagesController.prototype.page = function (name) {
	        for (var i = 0; i < this._pages.length; i++) {
	            if (this._pages[i].name == name) return this._pages[i];
	        }
	        return null;
	    };
	    Object.defineProperty(PagesController.prototype, "on404", {
	        set: function set(handler) {
	            this._on404 = handler;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Object.defineProperty(PagesController.prototype, "pathResolver", {
	        get: function get() {
	            return this._pathResolver;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    return PagesController;
	}();
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = PagesController;
	var Page = function () {
	    function Page(name, title) {
	        this._name = name;
	        this.title = title;
	    }
	    Object.defineProperty(Page.prototype, "name", {
	        get: function get() {
	            return this._name;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Object.defineProperty(Page.prototype, "onSelect", {
	        set: function set(listener) {
	            this._handlers.push(listener);
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Object.defineProperty(Page.prototype, "onHandle", {
	        set: function set(listener) {
	            this._interceptors.push(listener);
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Page.prototype._notifyOnSelect = function (data) {
	        if (data === void 0) {
	            data = null;
	        }
	        this._handlers.forEach(function (func) {
	            return func(data);
	        });
	    };
	    Page.prototype._notifyInterceptors = function (data) {
	        if (data === void 0) {
	            data = null;
	        }
	        return this._interceptors.every(function (interceptor) {
	            return interceptor(data);
	        });
	    };
	    return Page;
	}();
	exports.Page = Page;
	//# sourceMappingURL=PageController.js.map

/***/ },
/* 4 */
/***/ function(module, exports) {

	"use strict";
	
	var Router = function () {
	    function Router() {
	        var _this = this;
	        this._pathHandlers = new Array();
	        window.addEventListener('popstate', function () {
	            _this._routeWithoutPush();
	        }, false);
	    }
	    Router.prototype.route = function (path) {
	        history.pushState(path);
	        if (!this._routeWithoutPush()) location.reload();
	    };
	    Router.prototype._routeWithoutPush = function () {
	        return this._pathHandlers.some(function (handler) {
	            return handler();
	        });
	    };
	    Object.defineProperty(Router.prototype, "onRoute", {
	        set: function set(handler) {
	            this._pathHandlers.push(handler);
	        },
	        enumerable: true,
	        configurable: true
	    });
	    return Router;
	}();
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = Router;
	//# sourceMappingURL=Router.js.map

/***/ },
/* 5 */
/***/ function(module, exports) {

	"use strict";
	
	var HashPathResolver = function () {
	    function HashPathResolver() {}
	    HashPathResolver.prototype.resolve = function () {
	        return this.resolvePath(window.location.hash);
	    };
	    HashPathResolver.prototype.resolvePath = function (path) {
	        // check if path is empty
	        if (path.length == 0 || path.match(/^(\s|\/|\\)*$/)) return Array();
	        // get Path #page1#page2 like array: "#page1", "#page2"...
	        var hashList = path.match(/#.+?(?=#|$)/g);
	        // remove "#" symbol;
	        return hashList ? hashList.map(function (hash) {
	            return hash.substr(1);
	        }) : null;
	    };
	    HashPathResolver.prototype.getPageName = function () {
	        return this.getPageNameFrom(window.location.hash);
	    };
	    HashPathResolver.prototype.getPageNameFrom = function (path) {
	        // find last hash tag
	        var hash = path.match(/#(.+$)/);
	        return hash ? hash[1] : null;
	    };
	    HashPathResolver.prototype.pathToPage = function () {
	        var pages = [];
	        for (var _i = 0; _i < arguments.length; _i++) {
	            pages[_i - 0] = arguments[_i];
	        }
	        var link = "";
	        for (var i = 0; i < pages.length; i++) {
	            link += '#' + pages[i];
	        }return link;
	    };
	    return HashPathResolver;
	}();
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = HashPathResolver;
	//# sourceMappingURL=HashPathResolver.js.map

/***/ },
/* 6 */
/***/ function(module, exports) {

	"use strict";
	
	var SlashPathResolver = function () {
	    function SlashPathResolver(origin) {
	        if (origin === void 0) {
	            origin = '/';
	        }
	        this._origin = getStructure(origin);
	    }
	    SlashPathResolver.prototype.resolve = function () {
	        return this.resolvePath(window.location.pathname);
	    };
	    SlashPathResolver.prototype.resolvePath = function (path) {
	        return this._getPages(getStructure(path));
	    };
	    SlashPathResolver.prototype.getPageName = function () {
	        return this.getPageNameFrom(window.location.pathname);
	    };
	    SlashPathResolver.prototype.getPageNameFrom = function (path) {
	        var structure = this.resolvePath(path);
	        return structure.length > 0 ? structure[structure.length - 1] : null;
	    };
	    SlashPathResolver.prototype.pathToPage = function (pages) {
	        var link = "";
	        for (var i = 0; i < pages.length; i++) {
	            link += pages[i] + "/";
	        }var structure = getStructure(window.location.pathname);
	        var pagesInLocation = this._getPages(structure);
	        if (pagesInLocation == null) return null;
	        var path = "/";
	        for (var i = 0; i < structure.length - pagesInLocation.length; i++) {
	            path += structure[i] + "/";
	        }return window.location.host + path + link;
	    };
	    Object.defineProperty(SlashPathResolver.prototype, "origin", {
	        get: function get() {
	            return this._origin;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    SlashPathResolver.prototype._getPages = function (structure) {
	        if (this._origin.length == 0) return structure;
	        var match = 0;
	        var result = new Array();
	        for (var i = 0; i < structure.length; i++) {
	            if (match >= this._origin.length) {
	                result.push(structure[i]);
	            } else if (structure[i] == this._origin[match]) match++;
	        }
	        return match == this._origin.length ? result : null;
	    };
	    return SlashPathResolver;
	}();
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = SlashPathResolver;
	function getStructure(path) {
	    var structure = new Array();
	    path.replace('\\', '/').split(/\/+/g).forEach(function (dir) {
	        if (dir != '') structure.push(dir.trim());
	    });
	    return structure;
	}
	//# sourceMappingURL=SlashPathResolver.js.map

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var PageBinding_1 = __webpack_require__(8);
	var Animation_1 = __webpack_require__(1);
	var PagesContainer = function () {
	    function PagesContainer(name, defaultBinding) {
	        if (defaultBinding === void 0) {
	            defaultBinding = new PageBinding_1.default(null);
	        }
	        this._name = name;
	        this._defaultBinding = defaultBinding;
	        this._pages = new Array();
	    }
	    Object.defineProperty(PagesContainer.prototype, "name", {
	        get: function get() {
	            return this._name;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Object.defineProperty(PagesContainer.prototype, "pages", {
	        get: function get() {
	            return this._pages;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Object.defineProperty(PagesContainer.prototype, "defaultBinding", {
	        get: function get() {
	            return this._defaultBinding;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Object.defineProperty(PagesContainer.prototype, "currentPage", {
	        get: function get() {
	            return this._currentPage;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    PagesContainer.prototype.forEach = function (pageHandler) {
	        this._pages.forEach(pageHandler);
	    };
	    PagesContainer.prototype.page = function (name) {
	        for (var i = 0; i < this.pages.length; i++) {
	            if (this.pages[i].name == name) return this.pages[i];
	        }return null;
	    };
	    PagesContainer.prototype.addPage = function (page) {
	        this._pages.push(page);
	        page.load();
	    };
	    PagesContainer.prototype.removePage = function (name) {
	        for (var i = 0; i < this._pages.length; i++) {
	            if (this._pages[i].name == name) {
	                this._pages[i].unload();
	                this._pages.splice(i, 1);
	                return;
	            }
	        }
	    };
	    PagesContainer.prototype.switch = function (name) {
	        if (this.currentPage == null) return this.select(name);
	        var binding = this.currentPage.pageBindingFor(name);
	        var animation = binding ? binding.animation : this._defaultBinding.animation;
	        if (animation) this.selectWithAnimation(name, animation);else this.select(name);
	    };
	    PagesContainer.prototype.select = function (name) {
	        this.selectWithAnimation(name, Animation_1.Animation.empty());
	    };
	    PagesContainer.prototype.selectWithAnimation = function (name, animation) {
	        var _this = this;
	        var newPage = this.page(name);
	        if (newPage == null) throw new Error(noSuchPageErrorText(name, this.pages));
	        var currentPage = this.currentPage;
	        this.currentPage = newPage;
	        if (currentPage) {
	            // Check if user selects the save page before animation start
	            if (newPage.name == currentPage.name) return;
	            Animation_1.Animation.animate(currentPage.htmlElement, animation.leave, function () {
	                // Check if user not select the same page during animation
	                if (currentPage != _this.currentPage) currentPage.hide();
	            });
	        }
	        Animation_1.Animation.animate(newPage.htmlElement, animation.appear, undefined, function () {
	            return newPage.show();
	        }, animation.appearDelay);
	    };
	    return PagesContainer;
	}();
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = PagesContainer;
	function noSuchPageErrorText(userWantedPage, existedPages) {
	    var message = 'No such page. You try to switch to page \'' + userWantedPage + '\', so we have only ' + existedPages.length + ' pages: {\n';
	    var names = existedPages.map(function (item) {
	        return item.name;
	    }).reduce(function (names, pageName, i) {
	        var prev = i == 1 ? '  ' + names + ',\n' : names;
	        return prev + '  ' + pageName + ',\n';
	    });
	    return message + names + '}\n';
	}
	//# sourceMappingURL=PagesContainer.js.map

/***/ },
/* 8 */
/***/ function(module, exports) {

	"use strict";
	
	var PageBinding = function () {
	    function PageBinding(to, animation) {
	        if (animation === void 0) {
	            animation = null;
	        }
	        this._to = to;
	    }
	    Object.defineProperty(PageBinding.prototype, "to", {
	        get: function get() {
	            return this._to;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    return PageBinding;
	}();
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = PageBinding;
	//# sourceMappingURL=PageBinding.js.map

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	var PageBinding_1 = __webpack_require__(8);
	var Page = function () {
	    function Page(name, element) {
	        this._name = name;
	        this._element = element;
	        this._onSelectListeners = new Array();
	        this._onLeaveListeners = new Array();
	        this._onLoadListeners = new Array();
	        this._onUnloadListeners = new Array();
	        this._pageBindings = new Array();
	    }
	    Object.defineProperty(Page.prototype, "name", {
	        get: function get() {
	            return this._name;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Object.defineProperty(Page.prototype, "htmlElement", {
	        get: function get() {
	            return this._element;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Object.defineProperty(Page.prototype, "onLoad", {
	        set: function set(listener) {
	            this._onLoadListeners.push(listener);
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Object.defineProperty(Page.prototype, "onUnload", {
	        set: function set(listener) {
	            this._onUnloadListeners.push(listener);
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Object.defineProperty(Page.prototype, "onSelect", {
	        set: function set(listener) {
	            this._onSelectListeners.push(listener);
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Object.defineProperty(Page.prototype, "onLeave", {
	        set: function set(listener) {
	            this._onLeaveListeners.push(listener);
	        },
	        enumerable: true,
	        configurable: true
	    });
	    Page.prototype.forEach = function (pageBindingHandler) {
	        this._pageBindings.forEach(pageBindingHandler);
	    };
	    Page.prototype.to = function (pageName) {
	        var existedBinding = this.pageBindingFor(pageName);
	        return existedBinding ? existedBinding : new PageBinding_1.default(pageName);
	    };
	    Page.prototype.pageBindingFor = function (pageName) {
	        for (var i = 0; i < this._pageBindings.length; i++) {
	            if (this._pageBindings[i].to == pageName) return this._pageBindings[i];
	        }return null;
	    };
	    Page.prototype.load = function () {
	        this._notify(this._onLoadListeners);
	    };
	    Page.prototype.unload = function () {
	        this._notify(this._onUnloadListeners);
	    };
	    Page.prototype.show = function () {
	        this._element.style.visibility = 'inherit';
	        this._notify(this._onSelectListeners);
	    };
	    Page.prototype.hide = function () {
	        this._element.style.visibility = 'hidden';
	        this._notify(this._onLeaveListeners);
	    };
	    Page.prototype._notify = function (listeners) {
	        var _this = this;
	        listeners.forEach(function (listener) {
	            return listener(_this);
	        });
	    };
	    return Page;
	}();
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = Page;
	//# sourceMappingURL=Page.js.map

/***/ }
/******/ ]);
//# sourceMappingURL=pages.js.map