"use strict";
var PagesFactory_1 = require("../js/PagesFactory");
var Animation_1 = require("../js/core/Pages/Animation");
var ExploderParser_1 = require("../js/Exploder/ExploderParser");
var exploders = new ExploderParser_1.default();
var pages = PagesFactory_1.default.fromDocument(PagesFactory_1.default.hashMode());
pages.container('account').page('Sign Up').to('Sign In').animation =
    new Animation_1.Animation('fadeInLeft animated', 'fadeOutRight animated');
pages.container('main').page('Account Info').to('Authorization').animation =
    new Animation_1.Animation('rotateIn animated', 'fadeOutDown animated');
exploders.from(document);
var exploder = exploders.get("info");
exploder.onLoad = function () { return pages.parse(exploder.container); };
pages.container('main').page('Account Info').onSelect = function () { return exploder.explode(); };
pages.setIndex('Authorization#Sign In');
pages.start();
//# sourceMappingURL=bootstrap.js.map