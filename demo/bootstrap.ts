import PagesFactory from "../js/PagesFactory";
import Pages from "../js/Pages";
import {Animation} from "../js/core/Pages/Animation";
import ExplodersParser from "../js/Exploder/ExploderParser";
import Exploder from "../js/Exploder/Exploder";

const exploders = new ExplodersParser();

const pages: Pages = PagesFactory.fromDocument(PagesFactory.hashMode());

pages.container('account').page('Sign Up').to('Sign In').animation =
    new Animation('fadeInLeft animated', 'fadeOutRight animated');

pages.container('main').page('Account Info').to('Authorization').animation =
    new Animation('rotateIn animated', 'fadeOutDown animated');



exploders.from(document);
const exploder = exploders.get("info");
exploder.onLoad = () => pages.parse(exploder.container);
pages.container('main').page('Account Info').onSelect = () => exploder.explode();



pages.setIndex('Authorization#Sign In');
pages.start();